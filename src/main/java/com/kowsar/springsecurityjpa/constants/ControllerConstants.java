package com.kowsar.springsecurityjpa.constants;

public interface ControllerConstants {

    String HOME = "/";
    String USER = "/user";
    String ADMIN = "/admin";

}
