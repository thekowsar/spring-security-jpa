package com.kowsar.springsecurityjpa.controller;

import com.kowsar.springsecurityjpa.constants.ControllerConstants;
import com.kowsar.springsecurityjpa.dto.ResponseDto;
import com.kowsar.springsecurityjpa.model.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.kowsar.springsecurityjpa.constants.ControllerConstants.*;

@Slf4j
@RestController
public class HomeController {

    @GetMapping(value = HOME, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> home(){
        log.info("Request received for {}", HOME);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("Welcome Home")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", HOME, response);
        return response;
    }

    @GetMapping(value = USER, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> user(){
        log.info("Request received for {}", USER);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("Welcome User")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", USER, response);
        return response;
    }

    @GetMapping(value = ADMIN, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> admin(){
        log.info("Request received for {}", ADMIN);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("Welcome Admin")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", ADMIN, response);
        return response;
    }

}
