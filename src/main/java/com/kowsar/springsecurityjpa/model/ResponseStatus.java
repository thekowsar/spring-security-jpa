package com.kowsar.springsecurityjpa.model;

public enum ResponseStatus {

    OK,
    FAILED,
    CREATED,
    UPDATED,
    DELETED,
    NOT_FOUND

}
